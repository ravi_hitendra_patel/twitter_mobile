import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Icon from "react-native-vector-icons/FontAwesome";
import Feed from "./main/Feed";
import Profile from "./main/Profile";
import { useSelector } from "react-redux";
import Login from "./main/Login";
import People from "./main/People";
import EditProfileScreen from "./main/EditProfile";

const Tab = createBottomTabNavigator();

export default function Main() {
  const isLoggedIn = useSelector((state) => state.user.isLoggedIn);

  return !isLoggedIn ? (
    <Login />
  ) : (
    <Tab.Navigator
      initialRouteName="Feed"
      labeled={true}
      screenOptions={{
        tabBarActiveTintColor: "blue",
        tabBarInactiveTintColor: "black",
        headerShown: false,
      }}
    >
      <Tab.Screen
        name="Feed"
        component={Feed}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <Icon name="twitter" color={color} size={focused ? 26 : 21} />
          ),
        }}
      />
      <Tab.Screen
        name="People"
        component={People}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <Icon name="users" color={color} size={focused ? 26 : 21} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <Icon name="user" color={color} size={focused ? 26 : 21} />
          ),
        }}
      />
      <Tab.Screen
        name="Edit Profile"
        component={EditProfileScreen}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <Icon name="pencil" color={color} size={focused ? 26 : 21} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
