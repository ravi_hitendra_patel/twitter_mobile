import { View, Text, StyleSheet, Image } from "react-native";
import React from "react";
import CardContainer from "../../Utilities/CardContainer";
import Icon from "react-native-vector-icons/FontAwesome";
import { Avatar, Button } from "react-native-elements";
import globalStyles from "../../globalStyles";
import Ripple from "react-native-material-ripple";

export default function FriendCard({ currentPerson, onRemoveClicked }) {
  return (
    <CardContainer>
      <View style={styles.sectionContainer}>
        <View style={styles.avatarContainer}>
          <Avatar
            source={{
              uri: currentPerson?.profilePic.replace("http", "https"),
            }}
            rounded
            containerStyle={{ height: 40, width: 40 }}
          />
        </View>
        <View style={styles.friendInfoContainer}>
          <Text style={styles.textDesign}>{currentPerson?.name}</Text>
          <Text
            style={{
              ...styles.textDesign,
              color: globalStyles.lessBrightTextColor,
            }}
          >
            {currentPerson?.username}
          </Text>
        </View>
        <View style={styles.removeBtnContainer}>
          <Ripple
            rippleColor="white"
            style={styles.removeButton}
            onPress={() => onRemoveClicked(currentPerson)}
          >
            <Icon name="minus-circle" size={27} color="red" />
          </Ripple>
        </View>
      </View>
    </CardContainer>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    flexDirection: "row",
    width: 300,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
  },
  avatarContainer: {
    flex: 0.2,
    justifyContent: "center",
  },
  friendInfoContainer: {
    flex: 0.6,
    justifyContent: "center",
  },
  removeBtnContainer: {
    flex: 0.1,
    justifyContent: "flex-end",
  },
  textDesign: {
    fontSize: 16,
    fontWeight: "600",
    marginBottom: 5,
    color: globalStyles.brightTextColor,
  },
  removeButton: {
    width: "90%",
    alignSelf: "center",
  },
});
