import { View, FlatList, StyleSheet, ActivityIndicator } from "react-native";
import React from "react";
import Post from "./Post";
import globalStyles from "../../globalStyles";
import ModalMolecule from "../../Utilities/ModalMolecule";
import { useState } from "react";
import PostForm from "./PostForm";
import { Button } from "react-native-elements";
import Ripple from "react-native-material-ripple";
import axios from "axios";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addPostBundle } from "../../redux/actions/postActions";

export default function Feed() {
  const loadedPosts = useSelector((state) => state.posts.data);
  const [modalVisibility, setModalVisibility] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const getPosts = () => {
    setIsLoading(true);
    axios
      .get(
        `https://5fcaaa4b3c1c220016442a9f.mockapi.io/api/v1/tweets/?page=${currentPage}&limit=4`
      )
      .then((res) => res.data)
      .then((responseData) => {
        dispatch(addPostBundle(responseData));
        setIsLoading(false);
      });
  };

  const toggleModalVisibility = () => {
    setModalVisibility(!modalVisibility);
  };

  const renderLoader = () => {
    return isLoading ? (
      <View style={styles.loaderStyle}>
        <ActivityIndicator size="large" color="#aaa" />
      </View>
    ) : null;
  };

  const loadMoreItem = () => {
    setCurrentPage(currentPage + 1);
  };

  useEffect(() => {
    getPosts();
  }, [currentPage]);

  return (
    <View style={styles.sectionContainer}>
      <ModalMolecule visibility={modalVisibility}>
        <PostForm toggleModalVisibility={toggleModalVisibility} />
      </ModalMolecule>
      <Ripple
        rippleColor="white"
        style={styles.tweetButton}
        onPress={toggleModalVisibility}
      >
        <Button
          title="Tweet"
          titleStyle={{ fontWeight: "700" }}
          buttonStyle={{
            backgroundColor: "rgba(90, 154, 230, 1)",
            borderColor: "transparent",
            borderWidth: 0,
            borderRadius: 30,
          }}
        />
      </Ripple>
      <FlatList
        data={loadedPosts}
        renderItem={({ item }) => <Post post={item} />}
        ListFooterComponent={renderLoader}
        onEndReached={loadMoreItem}
        onEndReachedThreshold={0}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    paddingTop: 40,
    backgroundColor: globalStyles.screenBackground,
    flex: 1,
  },
  tweetButton: {
    width: "25%",
    alignSelf: "center",
    marginVertical: 25,
  },
  loaderStyle: {
    marginVertical: 16,
    alignItems: "center",
  },
});
