import { View, Text, StyleSheet, Platform } from "react-native";
import React from "react";
import CardContainer from "../../Utilities/CardContainer";
import Icon from "react-native-vector-icons/FontAwesome";
import { Avatar, Button } from "react-native-elements";
import globalStyles from "../../globalStyles";
import Ripple from "react-native-material-ripple";

export default function PeopleCard({ currentPerson, onConnectClick }) {
  return (
    <CardContainer>
      <View style={styles.sectionContainer}>
        <View style={styles.avatarAndConnectContainer}>
          <Avatar
            source={{
              uri: currentPerson?.profilePic.replace("http", "https"),
            }}
            rounded
            containerStyle={{ height: 100, width: 100 }}
          />
          <Ripple
            rippleColor="white"
            style={styles.connectButton}
            onPress={() => {
              onConnectClick(currentPerson);
            }}
          >
            <Button
              icon={<Icon name="plus" size={15} color="white" />}
              buttonStyle={{
                backgroundColor: "#246185",
                borderColor: "transparent",
                borderRadius: 10,
              }}
            />
          </Ripple>
        </View>
        <View style={styles.currentUserInfoContainer}>
          <View style={styles.personInfoContainer}>
            <Text style={styles.textDesign}>{currentPerson?.name}</Text>
            <Text
              style={{
                ...styles.textDesign,
                color: globalStyles.dullTextColor,
                fontSize: Platform.OS === "ios" ? 14 : 11,
                marginBottom: 10,
              }}
            >
              {currentPerson?.username}
            </Text>
            <Text
              style={{
                ...styles.textDesign,
                color: globalStyles.lessBrightTextColor,
                fontSize: Platform.OS === "ios" ? 11 : 9,
              }}
            >
              {currentPerson?.email}
            </Text>
            <Text
              style={{
                ...styles.textDesign,
                color: globalStyles.lessBrightTextColor,
                fontSize: Platform.OS === "ios" ? 11 : 9,
              }}
            >
              {currentPerson?.bio}
            </Text>
          </View>
          <View style={styles.followerFollowingSection}>
            <View style={styles.childSectionDesign}>
              <Text style={styles.textDesign}>{currentPerson?.followers}</Text>
              <Text
                style={{
                  ...styles.textDesign,
                  color: globalStyles.dullTextColor,
                  fontSize: Platform.OS === "ios" ? 11 : 9,
                }}
              >
                Followers
              </Text>
            </View>
            <View style={styles.childSectionDesign}>
              <Text style={styles.textDesign}>{currentPerson?.following}</Text>
              <Text
                style={{
                  ...styles.textDesign,
                  color: globalStyles.dullTextColor,
                  fontSize: Platform.OS === "ios" ? 11 : 9,
                }}
              >
                Following
              </Text>
            </View>
          </View>
        </View>
      </View>
    </CardContainer>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    flexDirection: "row",
    height: Platform.OS === "ios" ? 195 : 195,
  },
  avatarAndConnectContainer: {
    flex: 0.35,
    flexDirection: "column",
    padding: 10,
    alignItems: "center",
  },
  currentUserInfoContainer: {
    flex: 0.65,
    padding: 10,
    flexDirection: "column",
  },
  personInfoContainer: {
    flex: 0.6,
  },
  textDesign: {
    fontSize: Platform.OS === "ios" ? 16 : 13,
    fontWeight: "600",
    marginBottom: 5,
    color: globalStyles.brightTextColor,
  },
  followerFollowingSection: {
    flexDirection: "row",
    flex: 0.4,
    justifyContent: "center",
    alignItems: "center",
  },
  childSectionDesign: {
    flex: 0.5,
    padding: 10,
  },
  connectButton: {
    width: "50%",
    alignSelf: "center",
    marginTop: 20,
  },
});
