import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  ScrollView,
} from "react-native";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import globalStyles from "../../globalStyles";
import FriendCard from "./FriendCard";
import ModalMolecule from "../../Utilities/ModalMolecule";
import { removeFriend } from "../../redux/actions/friendsAction";
import { useState } from "react";
import Ripple from "react-native-material-ripple";
import { Button } from "react-native-elements";

export default function Profile({ navigation }) {
  const user = useSelector((state) => state.user.data);
  const friendsList = useSelector((state) => state.friends.friendsList);
  const [modalVisibility, setModalVisibility] = useState(false);
  const [clickedUser, setClickedUser] = useState({});
  const dispatch = useDispatch();

  const toggleModalVisibility = () => {
    setModalVisibility((prevState) => !prevState);
  };

  const handleConnectClick = (clickedFriend) => {
    toggleModalVisibility();
    setClickedUser(clickedFriend);
  };

  const removeClickedFriend = () => {
    dispatch(removeFriend(clickedUser));
    toggleModalVisibility();
  };

  return (
    <ScrollView
      style={{ flex: 1, backgroundColor: globalStyles.screenBackground }}
    >
      <View style={styles.sectionContainer}>
        <ModalMolecule visibility={modalVisibility}>
          <Text style={styles.modalMessage}>
            Would you like to remove {clickedUser.name} from your Friend List?!
          </Text>
          <View style={styles.modalButtonsContainer}>
            <Ripple
              rippleColor="white"
              style={styles.modalButton}
              onPress={toggleModalVisibility}
            >
              <Button
                title="Cancel"
                buttonStyle={{ backgroundColor: "pink" }}
              />
            </Ripple>
            <Ripple
              rippleColor="white"
              style={styles.modalButton}
              onPress={removeClickedFriend}
            >
              <Button
                title="Yes"
                buttonStyle={{ backgroundColor: "#507dde" }}
              />
            </Ripple>
          </View>
        </ModalMolecule>
        <View style={styles.avatarContainer}>
          <Image
            source={{
              uri: user?.profilePic.replace("http", "https"),
            }}
            style={styles.avatarDesign}
          />
        </View>
        <Text style={styles.sectionTitle}>{user?.name}</Text>
        <Text
          style={[
            styles.sectionDescription,
            { color: globalStyles.dullTextColor },
          ]}
        >
          {user?.username}
        </Text>
        <Text style={styles.sectionDescription}>{user?.email}</Text>
        <Text style={styles.sectionDescription}>{user?.bio}</Text>

        <View style={styles.followersFollowingSection}>
          <View
            style={{
              ...styles.followersFollowingSectionChild,
              ...styles.divider,
            }}
          >
            <Text style={styles.sectionTitle}>{user?.followers}</Text>
            <Text style={styles.sectionDescription}>Followers</Text>
          </View>
          <View style={styles.followersFollowingSectionChild}>
            <Text style={styles.sectionTitle}>{user?.following}</Text>
            <Text style={styles.sectionDescription}>Following</Text>
          </View>
        </View>

        <Text style={styles.sectionTitle}>Friends</Text>
        {!friendsList.length && (
          <Text style={{ ...styles.sectionDescription, color: "#7293db" }}>
            Visit People to make Connections
          </Text>
        )}
        {friendsList.map((friend) => (
          <FriendCard
            key={friend.id}
            currentPerson={friend}
            onRemoveClicked={handleConnectClick}
          />
        ))}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    alignItems: "center",
    flex: 1,
    backgroundColor: globalStyles.screenBackground,
    marginTop: 40,
    paddingTop: 20,
  },
  avatarContainer: {
    shadowColor: "white",
    shadowRadius: 15,
    shadowOpacity: 1,
  },
  avatarDesign: {
    height: Platform.OS === "ios" ? 150 : 120,
    width: Platform.OS === "ios" ? 150 : 120,
    marginBottom: Platform.OS === "ios" ? 15 : 0,
    borderRadius: 200,
  },
  sectionTitle: {
    fontSize: Platform.OS === "ios" ? 24 : 20,
    fontWeight: "600",
    marginTop: 15,
    color: globalStyles.brightTextColor,
  },
  sectionDescription: {
    marginVertical: Platform.OS === "ios" ? 8 : 4,
    fontSize: Platform.OS === "ios" ? 17 : 13,
    fontWeight: "400",
    color: globalStyles.lessBrightTextColor,
  },
  followersFollowingSection: {
    height: Platform.OS === "ios" ? 120 : 90,
    flexDirection: "row",
    marginVertical: Platform.OS === "ios" ? 25 : 15,
    marginHorizontal: 20,
    borderBottomWidth: 0.5,
    borderBottomColor: "#72727a",
    borderTopWidth: 0.5,
    borderTopColor: "#72727a",
    alignItems: "center",
  },
  followersFollowingSectionChild: {
    flex: 1 / 2,
    alignItems: "center",
    justifyContent: "center",
    height: "70%",
  },
  divider: {
    borderRightWidth: 0.5,
    borderRightColor: "#72727a",
  },
  modalMessage: {
    fontSize: 16,
    fontWeight: "600",
    marginTop: 20,
  },
  modalButtonsContainer: {
    flexDirection: "row",
    marginVertical: Platform.OS === "ios" ? 15 : 0,
  },
  modalButton: {
    flex: 0.5,
    alignContent: "center",
    alignSelf: "center",
    marginVertical: 25,
    marginHorizontal: 10,
  },
  editProfileButton: {
    flex: 0.9,
    alignContent: "center",
    alignSelf: "center",
    marginTop: 20,
  },
});
