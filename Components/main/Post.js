import { View, Text, StyleSheet } from "react-native";
import React from "react";
import CardContainer from "../../Utilities/CardContainer";
import Icon from "react-native-vector-icons/FontAwesome";
import { Avatar } from "react-native-elements";
import globalStyles from "../../globalStyles";
import { useDispatch } from "react-redux";
import { incrementLikes } from "../../redux/actions/postActions";
import moment from "moment";

export default function Post({ post }) {
  const dispatch = useDispatch();
  const formattedTime = moment(post?.createdAt).fromNow();

  return (
    <CardContainer>
      <View style={styles.avatarNameContainer}>
        <Avatar
          source={{
            uri: post?.userProfilePic.replace("http", "https"),
          }}
          rounded
          containerStyle={{ height: 40, width: 40 }}
        />
        <View style={{ marginLeft: 20 }}>
          <Text style={styles.createdByDesign}>{post?.createdBy}</Text>
          <Text style={styles.usernameDesign}>{post?.username}</Text>
        </View>
      </View>
      <Text style={styles.createdByDesign}>{post?.content}</Text>
      <View style={styles.likesCommentsContainer}>
        <Text style={styles.likesAndComments}>
          <Icon
            name={post?.liked ? "heart" : "heart-o"}
            size={18}
            color="#900"
            onPress={() => {
              dispatch(incrementLikes(post));
            }}
          />{" "}
          {`${post?.likes} likes`}
        </Text>
        <Text
          style={styles.likesAndComments}
        >{`${post?.comments} comments`}</Text>
      </View>
      <Text style={styles.createdAtDesign}>{formattedTime}</Text>
    </CardContainer>
  );
}

const styles = StyleSheet.create({
  avatarNameContainer: {
    flex: 1 / 5,
    flexDirection: "row",
    marginBottom: 15,
  },
  likesCommentsContainer: {
    flex: 1 / 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 7,
  },
  likesAndComments: {
    fontSize: 13,
    fontWeight: "600",
    color: globalStyles.lessBrightTextColor,
  },
  comments: {
    fontSize: 13,
    fontWeight: "600",
  },
  createdByDesign: {
    fontSize: 16,
    fontWeight: "600",
    marginBottom: 5,
    color: globalStyles.brightTextColor,
  },
  usernameDesign: {
    fontSize: 13,
    fontWeight: "600",
    color: globalStyles.dullTextColor,
  },
  createdAtDesign: {
    fontSize: 10,
    fontWeight: "400",
    color: globalStyles.dullTextColor,
    marginVertical: 7,
    alignSelf: "flex-end",
  },
});
