import { View, TextInput, StyleSheet } from "react-native";
import React from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addPost } from "../../redux/actions/postActions";
import { Button } from "react-native-elements";
import Ripple from "react-native-material-ripple";
import MIcon from "react-native-vector-icons/MaterialIcons";

export default function PostForm({ toggleModalVisibility }) {
  const [postdata, setPostdata] = useState({ content: "" });
  const user = useSelector((state) => state.user.data);
  const posts = useSelector((state) => state.posts.data);
  const dispatch = useDispatch();

  const handleSubmit = () => {
    if (postdata?.content === "") return;

    dispatch(
      addPost({
        ...postdata,
        createdAt: new Date(),
        comments: 0,
        createdBy: user?.name,
        likes: 0,
        userProfilePic: user?.profilePic,
        username: user?.username,
        id: posts?.length + 500,
      })
    );
    toggleModalVisibility();
  };

  return (
    <>
      <MIcon
        name="close"
        size={20}
        style={styles.closeButton}
        onPress={() => toggleModalVisibility()}
      />
      <TextInput
        placeholder="Tweet Something.."
        style={styles.textInput}
        value={postdata?.content}
        onChangeText={(content) => {
          setPostdata({ ...postdata, content });
        }}
      />
      <Ripple rippleColor="white" onPress={handleSubmit}>
        <Button title="Tweet" />
      </Ripple>
    </>
  );
}

const styles = StyleSheet.create({
  textInput: {
    fontSize: Platform.OS === "ios" ? 15 : 13,
    borderWidth: 1,
    borderColor: "#c7c7d1",
    padding: Platform.OS === "ios" ? 10 : 8,
    borderRadius: 10,
    width: "80%",
    marginBottom: 10,
  },
  closeButton: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: Platform.OS === "ios" ? 30 : 20,
    borderWidth: 0.5,
    borderColor: "#c7c7d1",
    padding: 5,
    borderRadius: 10,
    alignSelf: "flex-end",
  },
});
