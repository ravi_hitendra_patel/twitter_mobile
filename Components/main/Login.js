import { View, Image, TextInput, Alert, StyleSheet } from "react-native";
import React from "react";
import { useState, useEffect } from "react";
import Ripple from "react-native-material-ripple";
import { Button } from "react-native-elements";
import validateUser from "../../helpers/validateUser";
import { useDispatch } from "react-redux";
import useHttpGetRequest from "../../helpers/useHttpGetRequest";
import { setPeopleList } from "../../redux/actions/friendsAction";

export default function Login() {
  const { payLoad: users, loading: usersLoading } = useHttpGetRequest(
    "https://5fcaaa4b3c1c220016442a9f.mockapi.io/api/v1/users"
  );
  const dispatch = useDispatch();
  const [userList, setUserList] = useState([]);
  const [inputs, setInputs] = useState({ name: "", pincode: "" });

  useEffect(() => {
    if (!usersLoading) {
      setUserList(users);
    }
  }, [usersLoading]);

  const handleSubmit = () => {
    const userId = validateUser(inputs, userList, dispatch);

    if (!userId) {
      Alert.alert("Login Failed", "Login Credentials are not correct", [
        { text: "ok" },
      ]);
      setInputs({ ...inputs, name: "", pincode: "" });
    }
  };

  return (
    <View style={styles.screenDesign}>
      <Image
        // source={{
        //   uri: "https://www.freepnglogos.com/uploads/twitter-logo-png/twitter-logo-vector-png-clipart-1.png",
        // }}
        source={require("../../Assets/Twitter_Spash_Icon_PNG_-removebg-preview.png")}
        style={{ height: 200, width: 200, borderRadius: 200 }}
      />
      <TextInput
        placeholder="Enter Name..."
        value={inputs.name}
        onChangeText={(name) => setInputs({ ...inputs, name })}
        style={styles.textInput}
      />
      <TextInput
        placeholder="Enter Pincode..."
        value={inputs.pincode}
        onChangeText={(pincode) => setInputs({ ...inputs, pincode })}
        style={styles.textInput}
      />
      <Ripple rippleColor="white" onPress={handleSubmit}>
        <Button title="Login" />
      </Ripple>
    </View>
  );
}

const styles = StyleSheet.create({
  screenDesign: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  textInput: {
    fontSize: 15,
    borderWidth: 1,
    borderColor: "#c7c7d1",
    padding: 10,
    borderRadius: 10,
    width: "80%",
    marginBottom: 20,
  },
});
