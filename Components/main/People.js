import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Text,
} from "react-native";
import React from "react";
import globalStyles from "../../globalStyles";
import ModalMolecule from "../../Utilities/ModalMolecule";
import { useState } from "react";
import { Button } from "react-native-elements";
import Ripple from "react-native-material-ripple";
import axios from "axios";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addPeopleBundle, makeFriend } from "../../redux/actions/friendsAction";
import PeopleCard from "./PeopleCard";

export default function People() {
  const loadedPeopleList = useSelector((state) => state.friends.peopleList);
  const [modalVisibility, setModalVisibility] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  const [connectedUser, setConnectedUser] = useState({});
  const loggedInUser = useSelector((state) => state.user.data);

  const getPeople = () => {
    setIsLoading(true);
    axios
      .get(
        `https://5fcaaa4b3c1c220016442a9f.mockapi.io/api/v1/users/?page=${currentPage}&limit=3`
      )
      .then((res) => res.data)
      .then((responseData) => {
        dispatch(addPeopleBundle(responseData, loggedInUser));
        setIsLoading(false);
      });
  };

  const toggleModalVisibility = () => {
    setModalVisibility((prevState) => !prevState);
  };

  const renderLoader = () => {
    return isLoading ? (
      <View style={styles.loaderStyle}>
        <ActivityIndicator size="large" color="#aaa" />
      </View>
    ) : null;
  };

  const loadMoreItem = () => {
    setCurrentPage(currentPage + 1);
  };

  useEffect(() => {
    getPeople();
  }, [currentPage]);

  const handleConnectClick = (clickedUser) => {
    toggleModalVisibility();
    setConnectedUser(clickedUser);
  };

  const makeConnectedUserFriend = () => {
    dispatch(makeFriend(connectedUser));
    toggleModalVisibility();
  };

  return (
    <View style={styles.sectionContainer}>
      <ModalMolecule visibility={modalVisibility}>
        <Text style={styles.modalMessage}>
          Would you like to make {connectedUser.name} your Friend?!
        </Text>
        <View style={styles.modalButtonsContainer}>
          <Ripple
            rippleColor="white"
            style={styles.modalButton}
            onPress={toggleModalVisibility}
          >
            <Button
              title="Cancel"
              buttonStyle={{ backgroundColor: "#c72c5a" }}
            />
          </Ripple>
          <Ripple
            rippleColor="white"
            style={styles.modalButton}
            onPress={makeConnectedUserFriend}
          >
            <Button title="Ok" buttonStyle={{ backgroundColor: "green" }} />
          </Ripple>
        </View>
      </ModalMolecule>
      <Text style={styles.title}>Connect To The World</Text>
      <FlatList
        data={loadedPeopleList}
        renderItem={({ item }) => (
          <PeopleCard
            key={item.id}
            currentPerson={item}
            onConnectClick={handleConnectClick}
          />
        )}
        ListFooterComponent={renderLoader}
        onEndReached={loadMoreItem}
        onEndReachedThreshold={0}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    paddingTop: 40,
    backgroundColor: globalStyles.screenBackground,
    flex: 1,
  },
  loaderStyle: {
    marginVertical: 16,
    alignItems: "center",
  },
  modalMessage: {
    fontSize: 16,
    fontWeight: "600",
    marginTop: Platform.OS === "ios" ? 20 : 30,
    textAlign: Platform.OS === "ios" ? "" : "center",
  },
  modalButtonsContainer: {
    flexDirection: "row",
    marginBottom: Platform.OS === "ios" ? 0 : 10,
  },
  modalButton: {
    flex: 0.5,
    alignContent: "center",
    alignSelf: "center",
    marginVertical: 25,
    marginHorizontal: 10,
  },
  title: {
    marginVertical: 12,
    fontSize: 20,
    fontWeight: "500",
    color: "#a8bbe6",
    alignSelf: "center",
  },
});
