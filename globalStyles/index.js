const globalStyles = {
  screenBackground: "#030f29",
  brightTextColor: "white",
  lessBrightTextColor: "#b4b9c2",
  dullTextColor: "gray",
};

export default globalStyles;
