import { View, Text, StyleSheet } from "react-native";
import React from "react";

export default function CardContainer({ children }) {
  return <View style={styles.cardContainer}>{children}</View>;
}

const styles = StyleSheet.create({
  cardContainer: {
    margin: 15,
    padding: 15,
    borderRadius: 15,
    backgroundColor: "#020221",
    paddingBottom: 0,
    shadowColor: "white",
    shadowRadius: 5,
    shadowOpacity: 0.3,
  },
});
