import { View, Text, Modal, StyleSheet } from "react-native";
import React from "react";

export default function ModalMolecule({ children, visibility }) {
  return (
    <Modal transparent={true} visible={visibility}>
      <View style={styles.modalContent}>
        <View style={styles.modalBody}>{children}</View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#000000aa",
  },
  modalBody: {
    backgroundColor: "white",
    borderRadius: 10,
    height: Platform.OS === "ios" ? "20%" : "25%",
    width: "80%",
    padding: 20,
    alignItems: "center",
    justifyContent: "center",
  },
});
