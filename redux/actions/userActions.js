import { ActionTypes } from "../constants";

export const setUser = (user) => {
  return {
    type: ActionTypes.SET_USER,
    payload: user,
  };
};

export const setUserPosts = (userPosts) => {
  return {
    type: ActionTypes.SET_USER_POSTS,
    payload: userPosts,
  };
};
