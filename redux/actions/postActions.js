import { ActionTypes } from "../constants";

export const addPost = (post) => {
  return {
    type: ActionTypes.ADD_POST,
    payload: post,
  };
};

export const addPostBundle = (postBundle) => {
  return {
    type: ActionTypes.ADD_POST_BUNDLE,
    payload: postBundle,
  };
};

export const incrementLikes = (post) => {
  return {
    type: ActionTypes.INCREMENT_LIKES_COUNT_FOR_POST,
    payload: post,
  };
};
