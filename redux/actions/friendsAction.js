import { ActionTypes } from "../constants";

export const addPeopleBundle = (peopleBundle, loggedInUser) => {
  return {
    type: ActionTypes.ADD_PEOPLE_BUNDLE,
    payload: peopleBundle.filter((person) => person.id !== loggedInUser.id),
  };
};

export const makeFriend = (friendUser) => {
  return {
    type: ActionTypes.MAKE_FRIEND,
    payload: friendUser,
  };
};

export const removeFriend = (friendUser) => {
  return {
    type: ActionTypes.REMOVE_FRIEND,
    payload: friendUser,
  };
};
