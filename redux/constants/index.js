export const ActionTypes = {
  SET_USER: "SET_USER",
  SET_USER_POSTS: "SET_USER_POSTS",
  ADD_POST: "ADD_POST",
  ADD_POST_BUNDLE: "ADD_POST_BUNDLE",
  ADD_PEOPLE_BUNDLE: "ADD_PEOPLE_BUNDLE",
  MAKE_FRIEND: "MAKE_FRIEND",
  REMOVE_FRIEND: "REMOVE_FRIEND",
  INCREMENT_LIKES_COUNT_FOR_POST: "INCREMENT_LIKES_COUNT_FOR_POST",
};
