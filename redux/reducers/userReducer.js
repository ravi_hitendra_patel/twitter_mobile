import { ActionTypes } from "../constants";

const initialState = {
  data: {},
  isLoggedIn: false,
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SET_USER:
      return {
        ...state,
        data: action.payload,
        isLoggedIn: true,
      };
    case ActionTypes.SET_USER_POSTS:
      return {
        ...state,
        posts: action.payload,
      };

    default:
      return state;
  }
};
