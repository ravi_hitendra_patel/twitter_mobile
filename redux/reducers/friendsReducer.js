import { ActionTypes } from "../constants";

const initialState = {
  peopleList: [],
  friendsList: [],
};

export const friendsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.ADD_PEOPLE_BUNDLE:
      return {
        ...state,
        peopleList: [...state.peopleList, ...action.payload],
      };
    case ActionTypes.MAKE_FRIEND:
      return {
        ...state,
        peopleList: state.peopleList.filter(
          (person) => person.id !== action.payload.id
        ),
        friendsList: [action.payload, ...state.friendsList],
      };
    case ActionTypes.REMOVE_FRIEND:
      return {
        ...state,
        peopleList: [...state.peopleList, action.payload],
        friendsList: state.friendsList.filter(
          (person) => person.id !== action.payload.id
        ),
      };

    default:
      return state;
  }
};
