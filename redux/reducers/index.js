import { combineReducers } from "redux";
import { friendsReducer } from "./friendsReducer";
import { postReducer } from "./postReducer";
import { userReducer } from "./userReducer";

export const rootReducer = combineReducers({
  user: userReducer,
  posts: postReducer,
  friends: friendsReducer,
});
