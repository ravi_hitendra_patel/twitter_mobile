import { ActionTypes } from "../constants";

const initialState = {
  data: [],
};

export const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.ADD_POST:
      return {
        ...state,
        data: [action.payload, ...state.data],
      };
    case ActionTypes.ADD_POST_BUNDLE:
      return {
        ...state,
        data: [...state.data, ...action.payload],
      };
    case ActionTypes.INCREMENT_LIKES_COUNT_FOR_POST:
      return {
        ...state,
        data: state.data.map((post) => {
          if (post.id === action.payload.id && !post?.liked) {
            return { ...post, likes: post.likes + 1, liked: true };
          } else {
            return post;
          }
        }),
      };

    default:
      return state;
  }
};
