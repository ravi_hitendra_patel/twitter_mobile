import { setUser } from "../redux/actions/userActions";

const validateUser = (userDetails, users, dispatch) => {
  const { name, pincode } = userDetails;
  let userId = 0,
    currUserDetails;

  users.forEach((user) => {
    if (user.name === name && user.pincode === pincode) {
      userId = user.id;
      currUserDetails = user;
    }
  });

  if (userId > 0) {
    dispatch(setUser(currUserDetails));
  } else {
  }

  return userId;
};

export default validateUser;
